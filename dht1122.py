import sys
from report import Report

import Adafruit_DHT

SENSOR = 11
PIN = 4

def temp_humidity():
  humidity, temperature = Adafruit_DHT.read_retry(SENSOR, PIN)
  if humidity is not None and temperature is not None:
    response_data = []
    print(('Temp={0:0.1f}*  Humidity={1:0.1f}%'.format(temperature, humidity)))
    response_data.extend([
      { "name": "Temperatura", "value": '%.1f'% temperature, "unit":"°C"},
      { "name": "Humedad", "value": '%.1f'% humidity, "unit":"%"}
    ])
    return response_data
  else:
    print('Failed to get reading. Try again!')
    sys.exit(1)
    return False