import requests
from report import Report
from dht1122 import temp_humidity
from bmp180 import altitude_and_pressure
import time
# from prueba2 import pre
if __name__ == '__main__':
	URL = 'https://stationweather.herokuapp.com/api/v1/sensor_reports'
	FREQUENCY_SECONDS = 30
	iterador = True
	while iterador:
		sensors = []
		dht11 = temp_humidity()
		bmp180 = altitude_and_pressure()
		if dht11:
			sensors.extend(dht11)
		if bmp180:
			sensors.extend(bmp180)

		if len(sensors) > 0:
			info = {"sensor_report": sensors}
			response = requests.post(URL, json= info)
			if response.status_code == 200:
				time.sleep(FREQUENCY_SECONDS)
				continue
			else:
				iterador = False
		else:
			iterador = False




		
	# #payload = [{'name': 'Temperatura', 'value': '16', 'unit': '°C'}, {'name': 'Temperatura', 'value': '20', 'unit': '°C'} ]
	# info = {
	# 	'sensor_report': [
	# 		{'name': 'Temperatura', 'value': '24', 'unit': '°C'},{'name': 'Humedad', 'value': '50', 'unit': '%'}
	# 	]
	# }

	
	# response = requests.post(url, json=info)
	# print(response.url)

	# if response.status_code == 200:
	# 	print(response.content)