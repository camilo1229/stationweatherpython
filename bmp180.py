#!/usr/bin/python
import sys
from report import Report
import BMP085

# ===========================================================================
# Example Code
# ===========================================================================

# Initialise the BMP085 and use STANDARD mode (default value)
# bmp = BMP085(0x77, debug=True)
bmp = BMP085.BMP085()

# To specify a different operating mode, uncomment one of the following:
# bmp = BMP085(0x77, 0)  # ULTRALOWPOWER Mode
# bmp = BMP085(0x77, 1)  # STANDARD Mode
# bmp = BMP085(0x77, 2)  # HIRES Mode
# bmp = BMP085(0x77, 3)  # ULTRAHIRES Mode

temp = bmp.read_temperature()

# Read the current barometric pressure level
pressure = bmp.read_pressure()

# To calculate altitude based on an estimated mean sea level pressure
# (1013.25 hPa) call the function as follows, but this won't be very accurate
altitude = bmp.read_altitude()

# To specify a more accurate altitude, enter the correct mean sea level
# pressure level.  For example, if the current pressure level is 1023.50 hPa
# enter 102350 since we include two decimal places in the integer value
# altitude = bmp.readAltitude(102350)
def altitude_and_pressure():
  if altitude is not None and pressure is not None:
    response_data = []
    print(('Temp={0:0.1f}*  Altitude={1:0.1f}%'.format(temp, altitude)))
    response_data.extend([
      { "name": "Altitud", "value": round(altitude, 2), "unit":"mts"},
      { "name": "Presión Atmosférica", "value": round((pressure/100),2), "unit":"hPa"}
    ])
    return response_data
  else:
    print('Failed to get reading. Try again!')
    sys.exit(1)
    return False